﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GR.Core.Extensions;

namespace Level2
{
    class Program
    {
        static async Task Main(string[] args)
        {
            const string filePath = "C:\\Users\\nicol\\Downloads\\level2\\level2_5.in";
            if (!File.Exists(filePath))
            {
                Console.WriteLine("File not found");
            }

            var stringData = await File.ReadAllTextAsync(filePath);
            var data = stringData.Split("\r\n");
            var n = int.Parse(data[0]);

            var prices = data.ToList()
                .GetRange(1, n)
                .Select(int.Parse).ToArray();

            var m = int.Parse(data[prices.Length + 1]);

            var tasks = data.ToList()
                .GetRange(prices.Length + 2, m).ToArray();

            var result = new List<Dictionary<string, string>>();

            foreach (var taskPair in tasks)
            {
                var pair = taskPair.Split(" ");
                var taskId = int.Parse(pair[0]);
                var completionTime = int.Parse(pair[1]);

                var minSum = int.MaxValue;
                var index = 0;
                var minIndex = 0;

                while (index + completionTime < n + 1)
                {
                    var arr = prices.ToList().GetRange(index, completionTime);
                    var sum = arr.Sum();
                    if (sum < minSum)
                    {
                        minSum = sum;
                        minIndex = index;
                    }

                    index++;
                }
                result.Add(new Dictionary<string, string>
                {
                    { taskId.ToString(),  (minIndex).ToString() }
                });
            }

            Console.WriteLine(m);
            foreach (var pair in result)
            {
                foreach (var p in pair)
                {
                    Console.Write(p.Key + " " + p.Value);
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
