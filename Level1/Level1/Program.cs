﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Level1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            const string filePath = "C:\\Users\\nicol\\Downloads\\level1\\level1_5.in";
            if (!File.Exists(filePath))
            {
                Console.WriteLine("File not found");
            }

            var stringData = await File.ReadAllTextAsync(filePath);
            var data = stringData.Split("\r\n");
            var n = int.Parse(data[0]);

            var numbers = data.Where(x => !string.IsNullOrEmpty(x))
                .ToList()
                .GetRange(1, data.Length - 2)
                .Select(int.Parse).ToArray();

            var min = numbers[0];
            var minIndex = 0;
            for (var i = 1; i < n - 1; i++)
            {
                if (numbers[i] < min)
                {
                    min = numbers[i];
                    minIndex = i;
                }
            }
            Console.WriteLine("Min cost = " + min + " I = " + minIndex);
            Console.ReadKey();
        }
    }
}
