﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Level4
{
    class Program
    {
        static async Task Main(string[] args)
        {
            const string filePath = "C:\\Users\\nicol\\Downloads\\level4\\level4_example.in";
            if (!File.Exists(filePath))
            {
                Console.WriteLine("File not found");
            }

            var stringData = await File.ReadAllTextAsync(filePath);
            var data = stringData.Split("\r\n");

            var maxPower = int.Parse(data[0]);
            var maxCost = int.Parse(data[1]);
            var n = int.Parse(data[2]);

            var prices = data.ToList()
                .GetRange(3, n)
                .Select(int.Parse).ToArray();

            var m = int.Parse(data[prices.Length + 3]);

            var tasks = data.ToList()
                .GetRange(prices.Length + 4, m).ToArray();

            Console.WriteLine(m);

            foreach (var taskPair in tasks)
            {
                var pair = taskPair.Split(" ");
                var taskId = int.Parse(pair[0]);
                var power = int.Parse(pair[1]);
                var start = int.Parse(pair[2]);
                var end = int.Parse(pair[3]);

                var sumCost = 0;
                var total = 0;
                var random = new Random();
                while (sumCost <= maxCost)
                {
                    while (total < power)
                    {
                        total += random.Next(1, maxPower);
                        if (total > power)
                        {
                            total = 0;
                        }
                    }
                }
            }

            Console.ReadKey();
        }
    }
}
