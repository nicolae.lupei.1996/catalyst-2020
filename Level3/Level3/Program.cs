﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Level3
{
    class Program
    {
        static async Task Main(string[] args)
        {
            const string filePath = "C:\\Users\\nicol\\Downloads\\level3\\level3_5.in";
            if (!File.Exists(filePath))
            {
                Console.WriteLine("File not found");
            }

            var stringData = await File.ReadAllTextAsync(filePath);
            var data = stringData.Split("\r\n");
            var n = int.Parse(data[0]);

            var prices = data.ToList()
                .GetRange(1, n)
                .Select(int.Parse).ToArray();

            var m = int.Parse(data[prices.Length + 1]);

            var tasks = data.ToList()
                .GetRange(prices.Length + 2, m).ToArray();

            Console.WriteLine(m);
            foreach (var taskPair in tasks)
            {
                var pair = taskPair.Split(" ");
                var taskId = int.Parse(pair[0]);
                var power = int.Parse(pair[1]);
                var start = int.Parse(pair[2]);
                var end = int.Parse(pair[3]);
                var minIndex = int.MaxValue;
                var min = int.MaxValue;
                for (var i = start; i <= end; i++)
                {
                    if (prices[i] < min)
                    {
                        min = prices[i];
                        minIndex = i;
                    }
                }
                Console.WriteLine(taskId + " " + minIndex + " " + power);

            }

            Console.ReadKey();
        }
    }
}
